# AddCode Challenge

AddCode programming exercise in React/TypeScript for a Frontend Engineer position.

## Dependencies

First install project dependencies:

```
yarn install
```

## Development Server

Run development server with hot module reloading:

```
yarn run start
```

Open the browser to http://localhost:1234 and remember, the correct PIN is `1337`.

## Unit Tests

Run all unit tests with snapshot testing:

```
yarn run test
```

Open `./coverage/index.html` in your browser for pretty coverage.

## Production Build

To build for production run:

```
yarn run build
```

## Challenge Notes

-   I found the React documentation website a bit lacking in regards to unit testing and wasn't able to understand how to update the state and trigger a shallow rerender, which resulted in poor unit tests for the `<PinPad />` component due to time constraints. All other components, services and helpers are fully tested.
