// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
    preset: 'ts-jest/presets/js-with-ts',
    collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}', '!src/**/index.{js,jsx,ts,tsx}'],
    coverageReporters: ['html', 'text-summary'],
};
