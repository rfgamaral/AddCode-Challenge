const MAGIC_PIN = 1337;

export function requestMagicPin(): Promise<number> {
    return new Promise(resolve => {
        setTimeout(() => resolve(MAGIC_PIN), 500);
    });
}
