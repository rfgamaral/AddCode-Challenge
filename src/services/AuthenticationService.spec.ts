import { requestMagicPin } from './AuthenticationService';

describe('AuthenticationService', () => {
    beforeEach(() => {
        jest.useFakeTimers();
    });

    describe('#requestMagicPin', () => {
        it('should receive the correct magic pin from the request', () => {
            expect.assertions(1);

            const pendingPromise = requestMagicPin().then(magicPin => {
                expect(magicPin).toEqual(1337);
            });

            jest.advanceTimersByTime(500);

            return pendingPromise;
        });
    });
});
