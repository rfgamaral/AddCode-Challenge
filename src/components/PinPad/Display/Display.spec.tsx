import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

import Display, { DisplayStatus } from './Display';

describe('<Display />', () => {
    let shallowRenderer: ShallowRenderer.ShallowRenderer;

    beforeAll(() => {
        shallowRenderer = ShallowRenderer.createRenderer();
    });

    describe('#render', () => {
        describe('with status = Ready', () => {
            describe('and no pin', () => {
                it('should render correctly', () => {
                    shallowRenderer.render(<Display status={DisplayStatus.Ready} pin={[]} />);

                    expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
                });
            });

            describe('and pin = 1', () => {
                it('should render correctly', () => {
                    shallowRenderer.render(<Display status={DisplayStatus.Ready} pin={[1]} />);

                    expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
                });
            });

            describe('and pin = 13', () => {
                it('should render correctly', () => {
                    shallowRenderer.render(<Display status={DisplayStatus.Ready} pin={[1, 3]} />);

                    expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
                });
            });

            describe('and pin = 135', () => {
                it('should render correctly', () => {
                    shallowRenderer.render(
                        <Display status={DisplayStatus.Ready} pin={[1, 3, 5]} />
                    );

                    expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
                });
            });

            describe('and pin = 1358', () => {
                it('should render correctly', () => {
                    shallowRenderer.render(
                        <Display status={DisplayStatus.Ready} pin={[1, 3, 5, 7]} />
                    );

                    expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
                });
            });
        });

        describe('with status = Ok', () => {
            it('should render correctly', () => {
                shallowRenderer.render(<Display status={DisplayStatus.Ok} pin={[]} />);

                expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
            });
        });

        describe('with status = Error', () => {
            it('should render correctly', () => {
                shallowRenderer.render(<Display status={DisplayStatus.Error} pin={[]} />);

                expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
            });
        });

        describe('with status = Locked', () => {
            it('should render correctly', () => {
                shallowRenderer.render(<Display status={DisplayStatus.Locked} pin={[]} />);

                expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
            });
        });
    });
});
