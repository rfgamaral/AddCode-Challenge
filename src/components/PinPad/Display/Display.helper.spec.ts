import { concealPin } from './Display.helper';

describe('Display.helper', () => {
    describe('#concealPin', () => {
        describe('when pin is empty', () => {
            it('should return an empty string', () => {
                expect(concealPin([])).toEqual('');
            });
        });

        describe('when pin contains a single digit', () => {
            it('should return the single digit visible', () => {
                expect(concealPin([1])).toEqual('1');
            });
        });

        describe('when pin contains at most two digits', () => {
            it('should return the first digit concealed and the last one visible', () => {
                expect(concealPin([1, 3])).toEqual('•3');
            });
        });

        describe('when pin contains at least three digits', () => {
            it('should return the first two digits concelead and the last one visible', () => {
                expect(concealPin([1, 3, 5])).toEqual('••5');
            });
        });
    });
});
