import React from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';

import { concealPin } from './Display.helper';

export enum DisplayStatus {
    Ready = 'READY',
    Ok = 'OK',
    Error = 'ERROR',
    Locked = 'LOCKED',
}

interface Props {
    status: DisplayStatus;
    pin: number[];
}

const useStyles = makeStyles(theme => {
    const { h3 } = theme.typography;

    return {
        displayContainer: {
            margin: theme.spacing(1),
            padding: theme.spacing(2),
        },
        display: {
            height: `calc(${h3.fontSize} * ${h3.lineHeight})`,
            letterSpacing: theme.spacing(1),
            textAlign: 'center',
        },
    };
});

export default function Display(props: Props): JSX.Element {
    const classes = useStyles();

    const { status, pin } = props;

    return (
        <Paper className={classes.displayContainer}>
            <Typography className={classes.display} variant="h3" component="p">
                {status === DisplayStatus.Ready ? concealPin(pin) : status}
            </Typography>
        </Paper>
    );
}
