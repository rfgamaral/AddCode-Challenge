export function concealPin(pin: number[]): string {
    if (pin.length === 0) {
        return '';
    }

    return pin
        .reduce((accumulator: string[], cValue, index) => {
            accumulator[index] = index < pin.length - 1 ? '•' : cValue.toString();
            return accumulator;
        }, [])
        .join('');
}
