import { requestMagicPin } from '../../services/AuthenticationService';

export async function isPinValid(pin: number[]): Promise<boolean> {
    const magicPin = await requestMagicPin();
    return Number(pin.join('')) === magicPin;
}
