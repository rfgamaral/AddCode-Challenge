import React from 'react';

import Box from '@material-ui/core/Box';

import { makeStyles } from '@material-ui/core/styles';

import Button from './Button';

interface Props {
    disabled: boolean;
    onClick(digit: number): void;
}

const useStyles = makeStyles(theme => ({
    buttonContainer: {
        padding: theme.spacing(1),
        width: 'calc(100% / 3)',
        textAlign: 'center',
    },
}));

export default function Keypad(props: Props): JSX.Element {
    const classes = useStyles();

    const { disabled, onClick } = props;

    function handleClick(digit: number): void {
        onClick(digit);
    }

    return (
        <Box display="flex" flexWrap="wrap" justifyContent="center">
            <Box className={classes.buttonContainer}>
                <Button digit={7} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={8} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={9} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={4} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={5} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={6} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={1} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={2} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={3} disabled={disabled} onClick={handleClick} />
            </Box>
            <Box className={classes.buttonContainer}>
                <Button digit={0} disabled={disabled} onClick={handleClick} />
            </Box>
        </Box>
    );
}
