import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

import Keypad from './Keypad';

describe('<Keypad />', () => {
    let shallowRenderer: ShallowRenderer.ShallowRenderer;

    beforeAll(() => {
        shallowRenderer = ShallowRenderer.createRenderer();
    });

    describe('#render', () => {
        describe('when not disabled', () => {
            it('should render correctly', () => {
                shallowRenderer.render(<Keypad disabled={false} onClick={jest.fn} />);

                expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
            });
        });

        describe('when disabled', () => {
            it('should render correctly', () => {
                shallowRenderer.render(<Keypad disabled onClick={jest.fn} />);

                expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
            });
        });
    });

    describe('#handleClick', () => {
        const onClickMock = jest.fn();
        let renderOutput: React.ReactElement;

        beforeAll(() => {
            shallowRenderer.render(<Keypad disabled={false} onClick={onClickMock} />);
            renderOutput = shallowRenderer.getRenderOutput();
        });

        describe('for button with digit = 7', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button7 = renderOutput.props.children[0].props.children;
                button7.props.onClick(7);

                expect(onClickMock).toHaveBeenCalledWith(7);
            });
        });

        describe('for button with digit = 8', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button8 = renderOutput.props.children[1].props.children;
                button8.props.onClick(8);

                expect(onClickMock).toHaveBeenCalledWith(8);
            });
        });

        describe('for button with digit = 9', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button9 = renderOutput.props.children[2].props.children;
                button9.props.onClick(9);

                expect(onClickMock).toHaveBeenCalledWith(9);
            });
        });

        describe('for button with digit = 4', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button4 = renderOutput.props.children[3].props.children;
                button4.props.onClick(4);

                expect(onClickMock).toHaveBeenCalledWith(4);
            });
        });

        describe('for button with digit = 5', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button5 = renderOutput.props.children[4].props.children;
                button5.props.onClick(5);

                expect(onClickMock).toHaveBeenCalledWith(5);
            });
        });

        describe('for button with digit = 6', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button6 = renderOutput.props.children[5].props.children;
                button6.props.onClick(6);

                expect(onClickMock).toHaveBeenCalledWith(6);
            });
        });

        describe('for button with digit = 1', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button1 = renderOutput.props.children[6].props.children;
                button1.props.onClick(1);

                expect(onClickMock).toHaveBeenCalledWith(1);
            });
        });

        describe('for button with digit = 2', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button2 = renderOutput.props.children[7].props.children;
                button2.props.onClick(2);

                expect(onClickMock).toHaveBeenCalledWith(2);
            });
        });

        describe('for button with digit = 3', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button3 = renderOutput.props.children[8].props.children;
                button3.props.onClick(3);

                expect(onClickMock).toHaveBeenCalledWith(3);
            });
        });

        describe('for button with digit = 0', () => {
            it('should invoke onClick() callback with correct argument', () => {
                const button0 = renderOutput.props.children[9].props.children;
                button0.props.onClick(0);

                expect(onClickMock).toHaveBeenCalledWith(0);
            });
        });
    });
});
