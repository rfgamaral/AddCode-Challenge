import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

import Button from './Button';

describe('<Button />', () => {
    let shallowRenderer: ShallowRenderer.ShallowRenderer;

    beforeAll(() => {
        shallowRenderer = ShallowRenderer.createRenderer();
    });

    describe('#render', () => {
        describe('with digit and not disabled', () => {
            it('should render correctly', () => {
                shallowRenderer.render(<Button digit={4} disabled={false} onClick={jest.fn} />);

                expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
            });
        });

        describe('with digit and disabled', () => {
            it('should render correctly', () => {
                shallowRenderer.render(<Button digit={2} disabled onClick={jest.fn} />);

                expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
            });
        });
    });

    describe('#handleClick', () => {
        it('should invoke onClick() callback with correct argument', () => {
            const onClickMock = jest.fn();

            shallowRenderer.render(<Button digit={3} disabled={false} onClick={onClickMock} />);

            const renderOutput = shallowRenderer.getRenderOutput();
            renderOutput.props.onClick();

            expect(onClickMock).toHaveBeenCalledWith(3);
        });
    });
});
