import React from 'react';

import MuiButton from '@material-ui/core/Button';

import { makeStyles } from '@material-ui/core/styles';
import { yellow } from '@material-ui/core/colors';

interface Props {
    digit: number;
    disabled: boolean;
    onClick(digit: number): void;
}

const useStyles = makeStyles(theme => ({
    button: {
        backgroundColor: yellow[100],
        color: theme.palette.getContrastText(yellow[100]),
        borderRadius: '8px',
        fontSize: '3rem',
        lineHeight: 1.2,
        '&:hover': {
            backgroundColor: yellow[300],
            color: theme.palette.getContrastText(yellow[300]),
        },
        '&:active': {
            backgroundColor: yellow[900],
            color: 'white',
        },
    },
}));

export default function Button(props: Props): JSX.Element {
    const classes = useStyles();

    const { digit, disabled, onClick } = props;

    function handleClick(): void {
        onClick(digit);
    }

    return (
        <MuiButton
            className={classes.button}
            variant="contained"
            fullWidth
            disabled={disabled}
            onClick={handleClick}>
            {digit}
        </MuiButton>
    );
}
