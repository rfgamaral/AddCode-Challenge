import React, { useState } from 'react';

import Paper from '@material-ui/core/Paper';

import { lightBlue } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';

import Display, { DisplayStatus } from './Display';
import Keypad from './Keypad';

import { isPinValid } from './PinPad.helper';

interface State {
    displayStatus: DisplayStatus;
    displayPin: number[];
    keypadDisable: boolean;
    wrongAttempts: number;
}

const LOCKED_TIMEOUT = 30000; // 30 seconds

const useStyles = makeStyles(theme => ({
    pinPad: {
        padding: theme.spacing(3),
        backgroundColor: lightBlue[100],
        borderRadius: 30,
    },
}));

export default function PinPad(): JSX.Element {
    const classes = useStyles();

    const [pinPadState, setPinPadState] = useState<State>({
        displayStatus: DisplayStatus.Ready,
        displayPin: [],
        keypadDisable: false,
        wrongAttempts: 0,
    });

    function resetPinPadState(): void {
        setPinPadState(prevState => ({
            ...prevState,
            displayStatus: DisplayStatus.Ready,
            displayPin: [],
            keypadDisable: false,
            wrongAttempts: 0,
        }));
    }

    function setDisplayReady(): void {
        setPinPadState(prevState => ({
            ...prevState,
            displayStatus: DisplayStatus.Ready,
        }));
    }

    function setDisplayOk(): void {
        resetPinPadState();

        setPinPadState(prevState => ({
            ...prevState,
            displayStatus: DisplayStatus.Ok,
        }));
    }

    function setDisplayError(): void {
        setPinPadState(prevState => {
            let newDisplayStatus = DisplayStatus.Error;
            let newKeypadDisabled = false;

            const newWrongAttempts = prevState.wrongAttempts + 1;

            if (newWrongAttempts === 3) {
                newDisplayStatus = DisplayStatus.Locked;
                newKeypadDisabled = true;

                setTimeout(() => resetPinPadState(), LOCKED_TIMEOUT);
            }

            return {
                ...prevState,
                displayStatus: newDisplayStatus,
                displayPin: [],
                keypadDisable: newKeypadDisabled,
                wrongAttempts: newWrongAttempts,
            };
        });
    }

    function setDisplayPin(pin: number[]): void {
        setPinPadState(prevState => ({
            ...prevState,
            displayPin: pin,
        }));
    }

    function toggleKeypadDisabledState(): void {
        setPinPadState(prevState => ({
            ...prevState,
            keypadDisable: !prevState.keypadDisable,
        }));
    }

    async function handleClick(digit: number): Promise<void> {
        if (pinPadState.displayStatus !== DisplayStatus.Ready) {
            setDisplayReady();
        }

        const newPin = [...pinPadState.displayPin];
        newPin.push(digit);
        setDisplayPin(newPin);

        if (newPin.length === 4) {
            toggleKeypadDisabledState();

            if (await isPinValid(newPin)) {
                setDisplayOk();
            } else {
                setDisplayError();
            }
        }
    }

    return (
        <Paper className={classes.pinPad} elevation={5}>
            <Display status={pinPadState.displayStatus} pin={pinPadState.displayPin} />
            <Keypad disabled={pinPadState.keypadDisable} onClick={handleClick} />
        </Paper>
    );
}
