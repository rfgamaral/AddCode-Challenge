import { isPinValid } from './PinPad.helper';

jest.mock('../../services/AuthenticationService');

describe('PinPad.helper', () => {
    describe('#isPinValid', () => {
        describe('when the pin to validate is equal to the magic pin', () => {
            it('should return true', async () => {
                expect(await isPinValid([2, 4, 6, 8])).toEqual(true);
            });
        });

        describe('when the pin to validate is not equal to the magic pin', () => {
            it('should return false', async () => {
                expect(await isPinValid([1, 3, 5, 8])).toEqual(false);
            });
        });
    });
});
