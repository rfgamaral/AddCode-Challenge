import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

import PinPad from './PinPad';

describe('<PinPad />', () => {
    let shallowRenderer: ShallowRenderer.ShallowRenderer;

    beforeAll(() => {
        shallowRenderer = ShallowRenderer.createRenderer();
    });

    describe('#render', () => {
        describe('with initial state', () => {
            it('should render correctly', () => {
                shallowRenderer.render(<PinPad />);

                expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
            });
        });
    });
});
