import React from 'react';

import { hot } from 'react-hot-loader';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';

import { makeStyles } from '@material-ui/core/styles';

import PinPad from './PinPad';

const useStyles = makeStyles(theme => ({
    wrapper: {
        marginTop: theme.spacing(2),
    },
}));

function Application(): JSX.Element {
    const classes = useStyles();

    return (
        <>
            <CssBaseline />
            <Container className={classes.wrapper} maxWidth="sm">
                <PinPad />
            </Container>
        </>
    );
}

export default hot(module)(Application);
