import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

import Application from './Application';

describe('<Application />', () => {
    let shallowRenderer: ShallowRenderer.ShallowRenderer;

    beforeAll(() => {
        shallowRenderer = ShallowRenderer.createRenderer();
    });

    it('should render correctly', () => {
        shallowRenderer.render(<Application />);

        expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
    });
});
